"use strict";

var map = d3.map();
var oriW = 900;
var oriH = 450;
var zoomRatio = 1;

var width = parseInt(d3.select("#indonesia-map").style('width'));
var height = width / 2;
var oriScale = 1050 * (width / 900);

var projection = d3.geo.equirectangular().scale(oriScale).rotate([-118, 3]).translate([width / 2, height / 2]);

var path = d3.geo.path().projection(projection);

var svg = d3.select("#indonesia-map").append("svg").attr("width", width).attr("height", height);

var cities = {};
function getCities(id_provinsi, callback) {
    if (id_provinsi in cities) return callback(cities[id_provinsi]);

    $.ajax({
        url: '/index.php/helper/countries/index/' + id_provinsi,
        dataType: 'json',
        success: function success(data) {
            cities[id_provinsi] = data;
            callback(cities[id_provinsi]);
        }
    });
}

$.getJSON('/assets/topojson/indonesia.json', function (indonesia) {
    $.getJSON('/index.php/helper/topo', function (data) {
        var max = 0;
        var min = 999999;
        for (var d in data) {
            if (data[d] > max) max = data[d];
            if (data[d] < min) min = data[d];
        }

        var colors = "#fff5f0,#fee0d2,#fcbba1,#fc9272,#fb6a4a,#ef3b2c,#cb181d,#a50f15,#67000d".split(',');
        var q = d3.scale.quantize().domain([0, 1 / 9]).range(d3.range(9).map(function (i) {
            return colors[i];
        }));

        d3.select('#indonesia-map').append('div').attr('id', 'tooltip').style("position", "absolute").style("z-index", "10").style("display", "none");

        $('#tooltip').html('<strong></strong><ul id="city_list"></ul>');

        /** tool tip */
        var highlightColor = '#de2d26';
        var tip = {};
        tip.coor = function (d3mouse) {
            return {
                x: d3mouse[0] + 5,
                y: d3mouse[1] + -5
            };
        };
        tip.mousemove = function (d) {
            console.log("mousemove fired!");
            var points = tip.coor(d3.mouse(this));

            d3.select("#tooltip").style('top', points.y + "px").style('left', points.x + "px").style('display', 'block').text(d.properties.nm_provinsi);
        };

        tip.mouseover = function (d) {
            var $this = d3.select(this);
            var id_provinsi = d.properties.id_provinsi;
            // add prev attritbutes
            var prevStyles = {};
            prevStyles.fill = $this.style('fill');
            prevStyles.pointer = $this.style('pointer');

            // set to data-prevAttrs
            $this.attr('data-prevStyles', JSON.stringify(prevStyles));

            d3.select('#tooltip ul').html('');
            d3.select('#tooltip strong').html(d.properties.nm_provinsi + " <br> <small>Total message " + data[id_provinsi] + "</small>");
            // get cities
            getCities(d.properties.id_provinsi, function (data) {

                var $city_list = jQuery("#tooltip ul");
                data.forEach(function (item, idx) {
                    var li = "<li>" + item.city_name + "</li>";
                    console.log(li);
                    $city_list.append(li);
                });
            });

            $this.on('mousemove', function (d) {
                var points = tip.coor(d3.mouse(this));

                // set position and text
                d3.select("#tooltip").style('top', points.y + "px").style('left', points.x + "px");
            });

            // change fill path
            $this.style('fill', highlightColor);

            // show tooltip
            d3.select("#tooltip").style('display', 'block').style('pointer', 'pointer');
        };

        tip.mouseout = function (d) {
            var $this = d3.select(this);

            // set with prev attributes
            var prevStyles = JSON.parse($this.attr('data-prevStyles'));
            //console.log(prevAttrs);
            for (var attr in prevStyles) $this.style(attr, prevStyles[attr]);

            // hide tooltip
            d3.selectAll("#tooltip").style('display', 'none');
        };

        //d3.select('g').on('mouseout', tip.mouseout);
        svg.append("g").attr("class", "indonesia").selectAll("path").data(topojson.feature(indonesia, indonesia.objects.map).features).enter().append("path").on('mouseover', tip.mouseover)
        //.on('mousemove', tip.mousemove)
        .on('mouseout', tip.mouseout).style('fill', function (d) {
            var value = data[d.properties.id_provinsi];

            if (typeof value == "undefined") return '#f1f1f1';

            return q(value / max);
        }).attr("d", path).attr('stroke', 'rgba(0,0,0,.2)').attr('stroke-width', '0.5');
    });
});

function redrawMap(resizeWidth, resizeHeight, resizeScale) {
    // update projection
    projection.translate([resizeWidth / 2, resizeHeight / 2]).scale(resizeScale);

    // resize the map container
    d3.select('svg').style('width', resizeWidth + 'px').style('height', resizeHeight + 'px');

    // resize the map
    d3.selectAll('path').attr('d', path);
}

d3.select(window).on('resize', function () {
    var resizeWidth = parseInt(d3.select('#indonesia-map').style('width'));
    var resizeHeight = resizeWidth / width * height;
    var resizeScale = oriScale * (resizeWidth / width);

    redrawMap(resizeWidth, resizeHeight, resizeScale);
});

d3.select('.mapZoomIn').on('click', function () {
    var resizeWidth = parseInt(d3.select('#indonesia-map').style('width'));
    var resizeHeight = resizeWidth / width * height;
    var resizeScale = parseInt(oriScale * parseFloat(resizeWidth / width));

    zoomRatio = parseFloat(zoomRatio) + .1;
    resizeScale = parseInt(resizeScale * zoomRatio);

    redrawMap(resizeWidth, resizeHeight, resizeScale);
});

d3.select('.mapZoomOut').on('click', function () {
    var resizeWidth = parseInt(d3.select('#indonesia-map').style('width'));
    var resizeHeight = resizeWidth / width * height;
    var resizeScale = parseInt(oriScale * parseFloat(resizeWidth / width));

    zoomRatio = parseFloat(zoomRatio) - .1;
    resizeScale = parseInt(resizeScale * zoomRatio);

    redrawMap(resizeWidth, resizeHeight, resizeScale);
});

d3.select('.mapFit').on('click', function () {
    var resizeWidth = parseInt(d3.select('#indonesia-map').style('width'));
    var resizeHeight = resizeWidth / width * height;
    var resizeScale = parseInt(oriScale * parseFloat(resizeWidth / width));
    redrawMap(resizeWidth, resizeHeight, resizeScale);
});