
// test cases
var tc = [
	{data:'kaya OR miskin', ans:'valid'},
	{data:'kaya AND raya)', ans:'invalid'},
	{data:'(kaya AND raya)', ans:'valid'},
	{data:'kaya AND OR miskin', ans:'invalid'},
	{data:'"kaya raya" OR miskin', ans:'valid'},
	{data:'kaya raya OR miskin', ans:'invalid'},
	{data:'kaya raya" or miskin', ans:'invalid'},
];

var Validator = {};

// return true if parantheses valid
Validator.checkParentheses = function(string) {
	var counter = 0;
	var i = 0; 
	while(i < string.length){
		if (string[i] == '(') counter++;
		if (string[i] == ')') counter--;
		i++;
	}
	//console.log(`CHECKPARENTHESES ${string} : ${counter}`);

	if (counter == 0) return true;
	return false;
};

Validator.serializeQuery = function(string) {
	var serialize = [];

	var i = 0;
	var flag = 0;
	var tmpString = "";
	while (i < string.length) {
		if (string[i] == '"' && flag == 0) {
			flag = 1;
			i++;
			continue;
		}

		if (string[i] == '"' && flag == 1) {
			flag = 0;
			i++;
			serialize.push(tmpString);
			tmpString = '';
			continue;
		}

		if (flag == 1) {
			tmpString = tmpString + string[i];
			i++;
			continue;
		}

		if (string[i] == ' ' && tmpString != '') {
			serialize.push(tmpString);
			tmpString = '';
			i++;
			continue;
		}

		if (string[i] != ' ' && string[i] != '(' && string[i] != ')') {
			tmpString = tmpString + string[i];
			i++;
			continue;
		}

		i++;
	}

	if (tmpString != '')
		serialize.push(tmpString);

	return serialize;
}

Validator.check = function(string) {
	var answer = true;

	// if query string empty
	if (string.length == 0) return false;

	// if query mismatced parentheses
	if (this.checkParentheses(string) !== true) return false;

	// serialize query
	var serializedQuery = this.serializeQuery(string);

	// mode terms
	var mode = '';
	var i = 0;
	while(i < serializedQuery.length) {
		mode = i % 2 == 0? 'terms' : 'keyword';
		token = serializedQuery[i].toLowerCase().trim();

		if (mode == 'keyword' && token != 'or' && token != 'and'){
			answer = false;
			break;
		}

		if (mode == 'terms' && (token == 'or' || token == 'and')) {
			answer = false;
			break;
		}

		i++;
	}
	return answer;
};
