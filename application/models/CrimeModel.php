<?php 

/**
 * 
 */
 class CrimeModel extends CI_Model
 {
 	public function add($data)
 	{
 		return $this->db->insert('crime_history', $data);
 	}

 	public function detail($id)
 	{
 		return $this->db->select('*')->from('crime_history')->where('id', $id)->get()->row();
 	}

 	public function total_row()
 	{
 		return $this->db->select('count(*) as total')->from('crime_history')->get()->row()->total;
 	}

 	public function get($offset, $limit)
 	{
 		return $this->db->select('*')->from('crime_history')->limit($limit, $offset)->get();
 	}

 	public function update($id, $data)
 	{
 		$this->db->where('id', $id);
		$this->db->update('crime_history', $data);
 	}

 	public function delete($id)
 	{
 		$this->db->where('id', $id);
 		$this->db->delete('crime_history');
 	}
 } 