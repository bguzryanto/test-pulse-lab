<?php 

/**
* Countries
*/
class Countries extends CI_Controller
{

	public function index($id_provinsi)
	{
		$id_provinsi = (int) $id_provinsi;
		$cities = $this->db->select('city_name')
							->from('cities')
							->where('province_id', $id_provinsi)
							->order_by('city_name')
							->get();

		header('Content-Type: application/json');
		$this->output->set_output(json_encode($cities->result()));
	}
}