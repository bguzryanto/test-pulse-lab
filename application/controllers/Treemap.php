<?php 

class Treemap extends CI_Controller
{
	
	public function index()
	{
		$this->load->view('treemap');
	}

	public function json()
	{
		$this->output->set_output('{"children": [{"children": [{"id": 11, "name": "Ketahanan Pangan", "size": 2037}, {"id": 12, "name": "Harga Pangan", "size": 114}, {"id": 13, "name": "Kesehatan dan Gizi Pangan", "size": 104}], "name": "Food Suffeciency"}, {"children": [{"id": 14, "name": "Pemadaman Listrik", "size": 56}, {"id": 15, "name": "Energi Terbaharukan", "size": 813}, {"id": 16, "name": "Harga Energi", "size": 80}, {"id": 17, "name": "Ketahanan Energi", "size": 71}, {"id": 18, "name": "Subsidi Energi", "size": 187}], "name": "Energy"}, {"children": [{"id": 19, "name": "Industri Maritime", "size": 3}, {"id": 20, "name": "Infrastructure Maritime", "size": 45}], "name": "Maritime Development"}, {"children": [{"id": 21, "name": "Pembangunan ", "size": 4777}, {"id": 22, "name": "Infrastructure", "size": 4777}], "name": "Infrastructure Transportation"}, {"children": [{"id": 23, "name": "Kualitas Pendidikan", "size": 0}, {"id": 24, "name": "Beasiswa Pendidikan", "size": 0}], "name": "Education"}, {"children": [{"id": 25, "name": "Kualitas Kesehatan", "size": 3091}, {"id": 26, "name": "Biaya Kesehatan", "size": 193}], "name": "Health"}, {"children": [{"id": 27, "name": "Bantuan Pemerintah", "size": 3951}, {"id": 28, "name": "Tingkat Pengangguran", "size": 13}], "name": "Poverty Reduction"}, {"children": [{"id": 29, "name": "Pelayanan KTP & Akta", "size": 1613}, {"id": 30, "name": "Kepolisian", "size": 1944}], "name": "Bureucratic Reform"}, {"children": [{"id": 31, "name": "Makanan Khas", "size": 164}, {"id": 32, "name": "Pariwisata", "size": 36}], "name": "Tourism"}, {"children": [{"id": 33, "name": "Industri", "size": 1861}, {"id": 34, "name": "Hasil Industry", "size": 37}], "name": "Industry"}], "name": "Treemap"}');
	}

}