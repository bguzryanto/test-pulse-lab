<?php 

/**
* 
*/
class Crime extends CI_Controller
{
	public function index()
	{
		$this->page();
		#$this->output->enable_profiler(true);
	}

	public function page()
	{
		$this->load->library('pagination');

		$config['base_url'] = site_url('crime/index');
		$config['total_rows'] = $this->CrimeModel->total_row();
		$config['per_page'] = 10;

		$this->pagination->initialize($config);

		$uri_3 = $this->uri->segment(3);
		$data['items'] = $this->CrimeModel->get($uri_3, 10);

		$data['pagination_links'] = $this->pagination->create_links();
		$data['content'] = $this->load->view('components/pages/crime_index', $data, true);
		$this->load->view('page_template', $data);
	}

	public function add()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('fdate', 'Date', 'required');
		$this->form_validation->set_rules('ftime', 'Time', 'required');
		$this->form_validation->set_rules('report_by', 'Report By', 'required');
		$this->form_validation->set_rules('phone', 'Phone Number', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('location', 'Location/Address', 'required');
		$this->form_validation->set_rules('lat', 'Latitude', 'required');
		$this->form_validation->set_rules('long', 'Longitude', 'required');
		$this->form_validation->set_rules('desc', 'Description', 'required');

		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			if ($this->form_validation->run() !== FALSE) {
				$data = [
					'date_tanggal' => $this->input->post('fdate'),
					'date_waktu' => $this->input->post('ftime'),
					'report_by' => $this->input->post('report_by'),
					'phone_number' => $this->input->post('phone'),
					'email' => $this->input->post('email'),
					'location' => $this->input->post('location'),
					'lat'	=> $this->input->post('lat'),
					'long'	=> $this->input->post('long'),
					'crime_desc'	=> $this->input->post('desc'),
				];
				$this->CrimeModel->add($data);
				redirect(current_url());
			} else {
				echo validation_errors('<div class="error">', '</div>');
			}
		}
		else{
			$data['content'] = $this->load->view('components/pages/crime_add', null, true);
			$this->load->view('page_template', $data);
		}
	}

	public function edit($id)
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('fdate', 'Date', 'required');
		$this->form_validation->set_rules('ftime', 'Time', 'required');
		$this->form_validation->set_rules('report_by', 'Report By', 'required');
		$this->form_validation->set_rules('phone', 'Phone Number', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('location', 'Location/Address', 'required');
		$this->form_validation->set_rules('lat', 'Latitude', 'required');
		$this->form_validation->set_rules('long', 'Longitude', 'required');
		$this->form_validation->set_rules('desc', 'Description', 'required');

		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			if ($this->form_validation->run() !== FALSE) {
				$data = [
					'date_tanggal' => $this->input->post('fdate'),
					'date_waktu' => $this->input->post('ftime'),
					'report_by' => $this->input->post('report_by'),
					'phone_number' => $this->input->post('phone'),
					'email' => $this->input->post('email'),
					'location' => $this->input->post('location'),
					'lat'	=> $this->input->post('lat'),
					'long'	=> $this->input->post('long'),
					'crime_desc'	=> $this->input->post('desc'),
				];
				$this->CrimeModel->update($id, $data);
				redirect('crime');
			} else {
				echo validation_errors('<div class="error">', '</div>');
			}
		}
		else{
			$data['item'] = $this->CrimeModel->detail($id);
			$data['content'] = $this->load->view('components/pages/crime_update', $data, true);
			$this->load->view('page_template', $data);
		}

		
	}

	public function delete_all()
	{
		$ids = $this->input->get('ids');
		$ids = split(",", $ids);

		foreach ($ids as $id) {
			$this->CrimeModel->delete($id);
		}
		redirect('crime');
	}

	public function delete($id)
	{
		$this->CrimeModel->delete($id);
		redirect('crime');
	}
}