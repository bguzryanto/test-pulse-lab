<!DOCTYPE html>
<html>
<head>
	<title>Treemap</title>
	<?php $this->load->view('components/top_css'); ?>
	<style>
	.node {
		border: solid 1px white;
		font: 10px sans-serif;
		line-height: 12px;
		overflow: hidden;
		position: absolute;
		text-indent: 2px;
		padding: 5px;
		font-weight: bold;
	}

	#tooltip {
	  position: absolute;
	  width: 220px;
	  height: auto;
	  padding: 10px;
	  background-color: white;
	  pointer-events: none;
	}

	#tooltip.hidden {
	  display: none;
	}

	#tooltip p {
	  margin: 0;
	  font-family: sans-serif;
	  font-size: 16px;
	  line-height: 20px;
	}
</style>
</head>
<body role="document">
<script src="/bower_components/d3/d3.min.js"></script>
<?php $this->load->view('components/nav'); ?>
<div class="container" id="main"></div>
<script>
	var margin = {top: 40, right: 10, bottom: 10, left: 10},
	    width = 1150 - margin.left - margin.right,
	    height = 800 - margin.top - margin.bottom;

	var color = d3.scale.category20c();
	var treemap = d3.layout.treemap()
				    .size([width, height])
				    .sticky(true)
				    .value(function(d) { return d.size; });

	var div = d3.select("#main").append("div")
			    .style("position", "relative")
			    .style("width", (width + margin.left + margin.right) + "px")
			    .style("height", (height + margin.top + margin.bottom) + "px")
			    .style("left", margin.left + "px")
			    .style("top", margin.top + "px");

    var mousemove = function(d) {
        var xPosition = d3.event.pageX + 5;
        var yPosition = d3.event.pageY + 5;
        console.log(d);
        d3.select("#tooltip")
            .style("left", xPosition + "px")
            .style("top", yPosition + "px");
        
        d3.select("#tooltip #heading")
            .text(d['name']);
        
        d3.select("#tooltip #percentage")
            .text('Nilai : ' + d['size']);
        
        d3.select("#tooltip").classed("hidden", false);
    };

    var mouseout = function() {
      d3.select("#tooltip").classed("hidden", true);
    };


	d3.json("<?php echo site_url('treemap/json'); ?>", function(error, root) {
	  	if (error) 
	  		throw error;
	  
	  	var node = div.datum(root).selectAll(".node")
						.data(treemap.nodes)
						.enter().append("div")
						.attr("class", "node")
						.call(position)
						.style("background", function(d) { return d.children ? color(d.name) : null; })
						.text(function(d) { return d.children ? null : d.name; })
                        .on("mousemove", mousemove)
                        .on("mouseout", mouseout);


	  	d3.selectAll("input").on("change", function change() {
		    var value = this.value === "count" ? function() { return 1; } : function(d) { return d.size; };
		    node.data(treemap.value(value).nodes).transition().duration(1500).call(position);
		});
	});

function position() {
  this.style("left", function(d) { return d.x + "px"; })
      .style("top", function(d) { return d.y + "px"; })
      .style("width", function(d) { return Math.max(0, d.dx - 1) + "px"; })
      .style("height", function(d) { return Math.max(0, d.dy - 1) + "px"; });
}
</script>


<div id="tooltip" class="hidden">
	<p><strong id="heading"></strong></p>
	<p><span id="percentage"></span></p>
	<p><span id="revenue"></span></p>
</div>
	
	<?php $this->load->view('components/bot_scripts'); ?>
         
</body>
</html>