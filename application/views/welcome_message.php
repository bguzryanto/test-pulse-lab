<!DOCTYPE html>
<html>
<head>
	<title>Visualization</title>
	<?php include 'components/top_css.php'; ?>
</head>
<body role="document">
	
	<?php include 'components/nav.php'; ?>
	<?php include 'components/pages/home_p.php'; ?>
	
	<?php include 'components/bot_scripts.php'; ?>
	<script src="bower_components/topojson/topojson.js"></script>
	<script src="bower_components/datamaps/dist/datamaps.none.min.js"></script>
	<script>

    $.getJSON('index.php/helper/topo', function(dataJSON) {
        var map = new Datamap({
            element: document.getElementById('indonesia-map'),
            height: 280,
            responsive: true,
            scope: 'states_provinces',
            data:dataJSON,
            geographyConfig: {
                dataUrl: '/assets/topojson/indonesia.json',
                popupTemplate: function(geo, data) {
                    console.log(data);
                    return ['<div class="hoverinfo">',
                            '<strong>'+ geo.properties.name +'</strong><br/>',
                            'Number of things in ',
                            '</div>'].join('');
                }
            },
            fills: {
                HIGH: '#afafaf',
                LOW: '#123456',
                MEDIUM: 'blue',
                UNKNOWN: 'rgb(0,0,0)',
                defaultFill: '#f1f1f1'
            },
            data:{
                0: {
                    fillKey: 'LOW',
                    numberOfThings: 2002
                },
                1: {
                    fillKey: 'MEDIUM',
                    numberOfThings: 10381
                }
            },
            setProjection: function(element, options) {
                var projection = d3.geo.equirectangular()
                                    .scale(1050)
                                    .rotate([-118, 3])
                                    .translate([element.offsetWidth / 2, element.offsetHeight / 2]);

                var path = d3.geo.path()
                       .projection(projection);
                                                   
                return {path: path, projection: projection};
            },
            done: function() {
                $('.datamaps-subunit').each(function(i, el) {
                    el.setAttribute('class', 'datamaps-subunit province_' + i);
                });
            }
        });

        window.addEventListener('resize', function() {
            map.resize();
        });
    });

</script>
</body>
</html>