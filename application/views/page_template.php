<!DOCTYPE html>
<html>
<head>
	<title>Crime</title>
	<?php include 'components/top_css.php'; ?>
</head>
<body role="document">
	
	<?php $this->load->view('components/nav'); ?>
	
    <?php echo $content; ?>

	<?php $this->load->view('components/bot_scripts'); ?>
	
</body>
</html>