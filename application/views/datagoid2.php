<!DOCTYPE html>
<html>
<head>
	<title>Peserta Keluarga Berencana Aktif per Metode tahun 2012</title>
	<?php $this->load->view('components/top_css'); ?>
	<style>
	
</style>
</head>
<body role="document">
<script src="/bower_components/d3/d3.min.js"></script>
<?php $this->load->view('components/nav'); ?>
<div class="container" id="main">
	<div class="row">
		<div class="col-md-12">
	
			
		</div>
	</div>
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-10">
			<h3 class="page-title">Peserta Keluarga Berencana Aktif per Metode tahun 2012</h3>
			<div id="canvas-svg"></div>
		</div>
	</div>
</div>


<script>
	var SEGMENT = 'Metode Kb';
	var DATA = 'Jumlah Penguna';
	var data =  [
		{
			'Metode Kb': 'Kondom',
			'Jumlah Penguna': 16628
		},
		{
			'Metode Kb': 'Implan',
			'Jumlah Penguna': 12807
		},
		{
			'Metode Kb': 'IUD',
			'Jumlah Penguna': 7167
		},
		{
			'Metode Kb': 'MOP',
			'Jumlah Penguna': 921
		},
		{
			'Metode Kb': 'MOW',
			'Jumlah Penguna': 6332
		},
		{
			'Metode Kb': 'Pil',
			'Jumlah Penguna': 41180
		},
		{
			'Metode Kb': 'Suntikan',
			'Jumlah Penguna': 123636
		},
	];
	var total = 208671;
var width = 800,
    height = 800,
    radius = Math.min(width, height) / 3;

var color = d3.scale.ordinal()
    .range([
      '#d53e4f',
      '#fc8d59',
      '#fee08b',
      '#ffffbf',
      '#e6f598',
      '#99d594',
      '#3288bd',
    ]);



var arc = d3.svg.arc()
    .outerRadius(radius - 10)
    .innerRadius(0);

var pie = d3.layout.pie()
    .sort(null)
    .value(function(d) { return d[DATA]; });

var svg = d3.select("#canvas-svg").append("svg")
    .attr("width", width)
    .attr("height", height)
  .append("g")
    .attr("transform", "translate(" + width / 3 + "," + height / 2 + ")");

data.forEach(function(d) {
    d[DATA] = +d[DATA];
});

var g = svg.selectAll(".arc")
    .data(pie(data))
    .enter().append("g")
    .attr("class", "arc")
    .attr("fill", '#333');

var count = 0;

	g.append("path")
		.attr("d", arc)
		.attr("id", function(d) { return "arc-" + (count++); })
		.style("fill", function(d) {
		    return color(d.data[SEGMENT]);
		});

	g.append("text").attr("transform", function(d) {
    	return "translate(" + arc.centroid(d) + ")";
	}).attr("dy", ".35em").style("text-anchor", "middle")
	.text(function(d) {
	    return d.data[SEGMENT] + '(' + Math.round((d.data[DATA] / total) * 100) + '%)';
	});

	count = 0;
	var legend = svg.selectAll(".legend")
				    .data(data).enter()
				    .append("g").attr("class", "legend")
				    .attr("legend-id", function(d) {
				        return count++;
				    })
				    .attr("transform", function(d, i) {
				        return "translate(-50," + (-70 + i * 20) + ")";
				    })
				    .on("click", function() {
				        console.log("#arc-" + $(this).attr("legend-id"));
				        var arc = d3.select("#arc-" + $(this).attr("legend-id"));
				        arc.style("opacity", 0.3);
				        setTimeout(function() {
				            arc.style("opacity", 1);
				        }, 1000);
				    });

	legend.append("rect")
	    .attr("x", (width / 2) + 10)
	    .attr("width", 18).attr("height", 18)
	    .style("fill", function(d) {
	        return color(d[SEGMENT]);
	    });

	legend.append("text").attr("x", width / 2)
	    .attr("y", 9).attr("dy", ".35em")
	    .style("text-anchor", "end").text(function(d) {
	        return d[SEGMENT];
	    });
</script>
	
	<?php $this->load->view('components/bot_scripts'); ?>
         
</body>
</html>