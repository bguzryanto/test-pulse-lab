<!DOCTYPE html>
<html>
<head>
	<title>MDG Visualization Examples</title>
	<?php include 'components/top_css.php'; ?>
</head>
<body role="document">
	
	<?php $this->load->view('components/nav'); ?>
	<?php $this->load->view('components/pages/home_p'); ?>
	
	<?php $this->load->view('components/bot_scripts'); ?>
	<script src="/bower_components/topojson/topojson.js"></script>
    <script src="/assets/js/indonesia.js"></script>

</script>
</body>
</html>