<div class="container" id="main">
	<div class="col-md-10 col-md-offset-2">
		<h2 class="page-title">Add a Crime History</h2>
	</div>
	<div class="col-md-2">
	<?php $this->load->view('components/crime_nav'); ?>
	</div>
	<div class="col-md-10">
		<ol class="breadcrumb">
		  <li><a href="#">Home</a></li>
		  <li><a href="#">Master Data</a></li>
		  <li><a href="#">Crime History Data</a></li>
		  <li>Add Crime History</li>
		</ol>

		<div id="content">
		<?php echo validation_errors(); ?>
			<form action="<?php echo current_url(); ?>" method="post">
				<div class="row">
					<div class="col-md-3">
						<label for="fdate">Date of Incident</label>
						<input name="fdate" type="text" readonly="readonly" class="form-control datepicker" placeholder="Date of incident">	
					</div>
					<div class="col-md-3">
						<label for="fdate">Time of Incident</label>
						<input name="ftime" type="text" readonly="readonly" class="form-control timepicker" placeholder="Time of incident">	
					</div>
				</div>

				<div class="row">
					<div class="col-md-4">
						<label for="report-by">Report By</label>
						<input type="text" name="report_by" required class="form-control">
					</div>

					<div class="col-md-4">
						<label for="phone">Phone Number</label>
						<input type="number" name="phone" required class="form-control">
					</div>


					<div class="col-md-4">
						<label for="email">Email</label>
						<input type="email" name="email" required class="form-control">
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="location__address">
							<label for="address">Location</label>
							<textarea name="location" required id="address" cols="20" rows="7" class="form-control"></textarea>
						</div>
					</div>
					<div class="col-md-6">
						<div class="location__coordinate">
							<label for="lat">Latitude</label>
							<input name="lat" type="text" required class="form-control">
						</div>
						<div class="location__coordinate">
							<label for="long">Longitude</label>
							<input name="long" type="text" required class="form-control">
						</div>

						<div class="location__coordinate text-right">
							<button class="btn btn-info">Use Google Maps to Get Coordinate</button>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label for="desc">Crime Description</label>
						<textarea required name="desc" id="desc" class="form-control" cols="30" rows="10"></textarea>
					</div>
				</div>

				<div class="form__action text-right">
						<button type="submit" class="btn btn-success">Add Crime History</button>
				</div>
			</form>
		</div>
	</div>
</div>