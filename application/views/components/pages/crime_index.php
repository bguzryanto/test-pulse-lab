<div class="container" id="main">
	<div class="col-md-10 col-md-offset-2">
		<h2 class="page-title">Indonesian Crime History</h2>
	</div>
	<div class="col-md-2">
		<?php $this->load->view('components/crime_nav'); ?>
	</div>
	<div class="col-md-10">

		<ol class="breadcrumb">
		  <li><a href="#">Home</a></li>
		  <li><a href="#">Master Data</a></li>
		  <li class="active">Crime History Data</li>
		</ol>
		
		<div id="content">
			<div class="text-right">
				<div class="btn-group">
					<a href="#" class="btn btn-warning delete-checked">Delete All Checked Data</a>
				</div>
			</div>
			<table class="table table-striped">
		      <thead>
		        <tr>
		          <th>#</th>
		          <th>Report By</th>
		          <th>Phone Number</th>
		          <th>Email</th>
		          <th>Actions</th>
		        </tr>
		      </thead>
		      <tbody>
		      	<?php foreach ($items->result() as $item): ?>
					<tr>
			          <th scope="row">
			          	<input type="checkbox" data-id="<?php echo $item->id; ?>">&nbsp;
			          	<?php echo $item->id; ?>
			          </th>
			          <td><?php echo $item->report_by; ?></td>
			          <td><?php echo $item->phone_number; ?></td>
			          <td><?php echo $item->email; ?></td>
			          <td>
			          	<div class="btn-group">
			          		<a href="<?php echo site_url('crime/edit/' . (string)$item->id); ?>" class="btn btn-info btn-sm">Edit</a>
			          		<a href="<?php echo site_url('crime/delete/' . (string)$item->id); ?>" class="btn btn-danger btn-sm">Delete</a>
			          	</div>
			          </td>
			        </tr>
		      	<?php endforeach; ?>
		      </tbody>
		    </table>

		    <div class="pagination">
		    	<?php echo $pagination_links; ?>
		    </div>
		</div>

	</div>
</div>