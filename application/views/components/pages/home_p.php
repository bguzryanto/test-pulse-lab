<div class="container" id="main">
	<div class="col-md-2">
	</div>
	<div class="col-md-10">
		<h2 class="page-title">Welcome</h2>
		<ol class="breadcrumb">
		  <li><a href="#">Home</a></li>
		  <li><a href="#">Library</a></li>
		  <li class="active">Data</li>
		</ol>

		<div id="content">

			<h4>Query Checker</h4>
			<div class="input-group"><span class="input-group-addon" id="basic-addon2">?</span>
			  <input type="text" id="qcheck" class="form-control" placeholder="example: kaya AND miskin" aria-describedby="basic-addon2">
			</div>

			<br>
			
			<div id="indonesia-map">
				<div id="map-control">
					<div class="btn-group btn-group-sm float-right" role="group">
						<button href="#" class="btn btn-default mapZoomIn"><span class="glyphicon glyphicon-zoom-in"></span>&nbsp;Zoom-in</button>
						<button href="#" class="btn btn-default mapZoomOut"><span class="glyphicon glyphicon-zoom-out"></span>&nbsp;Zoom-out</button>
						<button href="#" class="btn btn-default mapFit"><span class="glyphicon glyphicon-fullscreen"></span>&nbsp;Fit Map</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>