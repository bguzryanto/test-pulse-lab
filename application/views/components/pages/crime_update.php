<div class="container" id="main">
	<div class="col-md-10 col-md-offset-2">
		<h2 class="page-title">Update a Crime History</h2>
	</div>
	<div class="col-md-2">
	<?php $this->load->view('components/crime_nav'); ?>
	</div>
	<div class="col-md-10">
		<ol class="breadcrumb">
		  <li><a href="#">Home</a></li>
		  <li><a href="#">Master Data</a></li>
		  <li><a href="#">Crime History Data</a></li>
		  <li>Add Crime History</li>
		</ol>

		<div id="content">
		<?php echo validation_errors(); ?>
			<form action="<?php echo current_url(); ?>" method="post">
				<div class="row">
					<div class="col-md-3">
						<label for="fdate">Date of Incident</label>
						<input name="fdate" type="text" readonly="readonly"  value="<?php echo $item->date_tanggal; ?>" class="form-control datepicker" placeholder="Date of incident">	
					</div>
					<div class="col-md-3">
						<label for="fdate">Time of Incident</label>
						<input name="ftime" value="<?php echo $item->date_waktu; ?>" type="text" readonly="readonly" class="form-control timepicker" placeholder="Time of incident">	
					</div>
				</div>

				<div class="row">
					<div class="col-md-4">
						<label for="report-by">Report By</label>
						<input type="text" value="<?php echo $item->report_by; ?>" name="report_by" required class="form-control">
					</div>

					<div class="col-md-4">
						<label for="phone">Phone Number</label>
						<input type="number" value="<?php echo $item->phone_number; ?>" name="phone" required class="form-control">
					</div>


					<div class="col-md-4">
						<label for="email">Email</label>
						<input type="email" value="<?php echo $item->email; ?>" name="email" required class="form-control">
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="location__address">
							<label for="address">Location</label>
							<textarea name="location" required id="address" cols="20" rows="7" class="form-control"><?php echo $item->location; ?></textarea>
						</div>
					</div>
					<div class="col-md-6">
						<div class="location__coordinate">
							<label for="lat">Latitude</label>
							<input name="lat" type="text" value="<?php echo $item->lat; ?>" required class="form-control">
						</div>
						<div class="location__coordinate">
							<label for="long">Longitude</label>
							<input name="long" type="text" required class="form-control"  value="<?php echo $item->long; ?>">
						</div>

						<div class="location__coordinate text-right">
							<button class="btn btn-info">Use Google Maps to Get Coordinate</button>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label for="desc">Crime Description</label>
						<textarea required name="desc" id="desc" class="form-control" cols="30" rows="10"><?php echo $item->crime_desc; ?></textarea>
					</div>
				</div>
				<div class="form__action text-right">
						<button type="submit" class="btn btn-success">Update Crime History</button>
				</div>
			</form>
		</div>
	</div>
</div>