
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link rel="stylesheet" type="text/css" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
 <link rel="stylesheet" href="/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />

<link rel="stylesheet" href="/assets/css/custom.css">
<link href='https://fonts.googleapis.com/css?family=Open+Sans|Roboto:400,700' rel='stylesheet' type='text/css'>

<script src="/bower_components/d3/d3.min.js"></script>
<script src="/assets/js/topojson.v1.min.js"></script>