<nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">Indonesian MDG Visualization</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="/">Home</a></li>
             <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Master Data <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo site_url('crime'); ?>">Crime History</a></li>
              </ul>
            </li>
            <li><a href="<?php echo site_url('datagoid1'); ?>">Data.go.id #1</a></li>
            <li><a href="<?php echo site_url('datagoid2'); ?>">Data.go.id #2</a></li>
            <li><a href="<?php echo site_url('treemap'); ?>">Treemap</a></li>
            
           
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#"><span class="glyphicon glyphicon-menu-hamburger"></span></a></li>  
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>