<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/bower_components/moment/min/moment.min.js"></script>
<script type="text/javascript" src="/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="/assets/js/QChecker.js"></script>
<script>
	$(function(){
		$('.timepicker').datetimepicker({
            format: 'LT',
            ignoreReadonly: true,
        });
        $('.datepicker').datetimepicker({
            format: 'DD/MM/YYYY',
            ignoreReadonly: true,
        });
        
        $('.delete-checked').on('click', function(){
            id = [];
            $('table input:checked').each(function(i, el){
                id.push($(el).data('id'));
            });
            id = id.join(',');

            window.location.href = '<?php echo site_url("crime/delete_all?ids='+id+'"); ?>';
        });      

        $('#qcheck').on('input', function(){
            var res = Validator.check($('#qcheck').val());
            if (res)
                $('#basic-addon2').html("&nbsp;&nbsp;Valid");
            else
                $('#basic-addon2').text('Invalid');
        });
	});
</script>