<!DOCTYPE html>
<html>
<head>
	<title>Data.go.id</title>
	<?php $this->load->view('components/top_css'); ?>
	<style>
	
</style>
</head>
<body role="document">
<script src="/bower_components/d3/d3.min.js"></script>
<?php $this->load->view('components/nav'); ?>
<div class="container" id="main">
	<h3 class="page-title">Proyeksi kelahiran 2010 - 2035</h3>
	<div id="canvas-svg"></div>
</div>


<script>
	var data = [
		{
			'years': 2010,
			'birth': 88.23
		},
		{
			'years': 2015,
			'birth': 83.94
		},
		{
			'years': 2020,
			'birth': 80.03
		},
		{
			'years': 2025,
			'birth':75.7
		},
		{
			'years': 2030,
			'birth': 71.73
		},
		{
			'years': 2035,
			'birth': 68.1
		},
	];
	var Y_DATA_FORMAT = d3.format("");

var margin = {top: 40, right: 40, bottom: 40, left: 40},
    width = 640 - margin.left - margin.right,
    height = 300 - margin.top - margin.bottom;

var x = d3.scale.ordinal()
    .rangeRoundBands([0, width], 0.1);

var y = d3.scale.linear()
    .range([height, 0]);

var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom");

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left");

var svg = d3.select("#canvas-svg").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

x.domain(data.map(function(d) { return d['years']; }));
y.domain([0, d3.max(data, function(d) { return d['birth']; })]);

svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis);

svg.append("g")
    .attr("class", "y axis")
    .call(yAxis)
  .append("text")
    .attr("transform", "rotate(-90)")
    .attr("y", 1)
    .attr("dy", ".71em")
    .style("text-anchor", "end")
    .text("Jumlah Kelahiran");

svg.selectAll(".bar")
    .data(data)
  .enter().append("rect")
    .attr("class", "bar")
    .attr("x", function(d) { return x(d['years']); })
    .attr("width", x.rangeBand())
    .attr("y", function(d) { return y(d['birth']); })
    .attr("height", function(d) { return height - y(d['birth']); })
    .attr("fill", '#2ca25f');
    
function type(d) {
  d['birth'] = +d['birth'];
  return d;
}


</script>
	
	<?php $this->load->view('components/bot_scripts'); ?>
         
</body>
</html>